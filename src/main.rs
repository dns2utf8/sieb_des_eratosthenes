fn main() {
    let list = sieb_simple(100_000_000);

    let mut nth = 0;
    list.iter()
        .enumerate()
        .filter(|(_, p)| **p == 0)
        //.filter(|(i, _)| better_fermat(i))
        .for_each(|(i, _)| {
            if nth >= 16 {
                println!("");
                nth = 1;
            } else {
                nth += 1;
            }
            print!("{:5} ", i);
        });

    println!("");
}

fn sieb_simple(max: usize) -> Vec<u8> {
    let mut sieb = vec![0; max];

    sieb[0] = 1;
    sieb[1] = 1;

    let mut prim = 2;
    loop {
        //println!("loop::prim = {}", prim);
        if prim * prim > max {
            return sieb;
        }
        let mut i = prim * 2;
        while i < max {
            sieb[i] = 1;
            i += prim;
        }
        // nächste
        prim += 1;
        while sieb[prim] == 1 {
            prim += 1;
        }
    }
}

fn better_fermat(i: &usize) -> bool {
    let first_primes = [
        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89,
        97, 101, 103, 107, 109, 113, 127, 131,
    ];

    for a in &first_primes {
        if is_fermat_prime(*a, *i) == false {
            eprintln!("found unconfirmed prime {}", i);
            return false;
        }
    }
    true
}

fn is_fermat_prime(a: usize, n: usize) -> bool {
    if a >= n {
        return true;
    }

    use num_bigint::BigUint;
    use num_traits::One;

    let a = BigUint::from(a);
    let n1 = BigUint::from(n - 1);
    let n = BigUint::from(n);

    a.modpow(&n1, &n).is_one()
}
